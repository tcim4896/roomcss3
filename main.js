var cl = console.log;
function px(val){
	return val + "px";
}
var flag = document.createElement("div");
flag.style.backgroundColor = "rgba(0,0,0,1)";
flag.style.position = "relative";

// 0
// 0,0 0,20 0,40 0,60
// 10
// 10,10 10,30 10,50
// ..
var square = document.createElement("div");
square.style.width = "10px";
square.style.height = "10px";
square.style.backgroundColor = "white";
square.style.position = "absolute";
square.style.zIndex = "999";

var pos = {y:0,x:0};
var row = 0;

for(let i = 1; i <= (7*5); i++){
	 var s = square.cloneNode(true);
	 s.style.top = px(pos.y);
	 s.style.left = px(pos.x);
	 pos.x += 10;
  	
  	if(i % 2 === 0){
  		flag.appendChild(s);
  	}
  	
  	if(i % 7 === 0){
  		  pos.y += 10;
  		  pos.x = 0;
  		}
}

var cube = document.getElementById("cube");

var faces = ["front", "right", "left", "top", "back", "top", "bottom"];

faces.forEach(face => {
  var f = flag.cloneNode(true);
  f.setAttribute("class", "cube__face  cube__face--" + face); 
  cube.appendChild(f);
});